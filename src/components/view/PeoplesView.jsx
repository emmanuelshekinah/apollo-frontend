import React, { useEffect } from "react";

const PeoplesView = (props) => {
  return (
    <React.Fragment>
      <input
        type="text"
        className="form-control"
        placeholder="Search..."
        onChange={(e) => props.searchPeoples(e.target.value)}
      />
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Height</th>
            <th scope="col">Mass</th>
            <th scope="col">Gender</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {props.peoples.map((data, key) => (
            <tr>
              <td>{data.name}</td>
              <td>{data.height}</td>
              <td>{data.mass}</td>
              <td>{data.gender}</td>
              <td>
                <button
                  type="button"
                  className="btn btn-outline-primary"
                  onClick={() => {
                    props.openDetails(data);
                  }}
                >
                  View Details
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="row">
        <div className="col-12 text-center">
          <nav aria-label="Page navigation example">
            <ul class="pagination">
              {props.prev !== null && (
                <li class="page-item">
                  <a
                    class="page-link"
                    href="#"
                    onClick={() => {
                      props.paginate(props.prev);
                    }}
                  >
                    Previous
                  </a>
                </li>
              )}

              {props.next !== null && (
                <li class="page-item">
                  <a
                    class="page-link"
                    href="#"
                    onClick={() => {
                      props.paginate(props.next);
                    }}
                  >
                    Next
                  </a>
                </li>
              )}
            </ul>
          </nav>
        </div>
      </div>
    </React.Fragment>
  );
};

export default PeoplesView;
