import React, { useEffect } from "react";
import { useSelector } from "react-redux";

const PersonView = (props) => {
  const obj = useSelector((state) => state.obj);

  useEffect(() => {
    console.log(obj);
  }, []);

  const style = {
    // backgroundColor: 'gray'
  };
  return (
    <React.Fragment>
      <div className="jumbotron jumbotron-fluid rounded" style={style}>
        {obj !== null && obj !== undefined && (
          <div className="container">
            <h1 className="display-4">{obj.name}</h1>
            <p className="lead">
              Height: {obj.height}
              <br />
              Mass: {obj.mass}
              <br />
              Gender: {obj.gender}
              <br />
            </p>
          </div>
        )}

        <button
          type="button"
          class="btn btn-secondary"
          onClick={() => {
            props.cancel();
          }}
        >
          Back
        </button>
      </div>
    </React.Fragment>
  );
};

export default PersonView;
