import React from "react";
import PersonController from "./controller/PersonController";

const Person = (props) => {

    const style = {
        padding: '50px'
    }
  return (
    <React.Fragment >
      <PersonController style={style}/>
    </React.Fragment>
  );
};

export default Person;
