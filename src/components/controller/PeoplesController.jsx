import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import PeoplesView from "../view/PeoplesView";
import {useDispatch} from 'react-redux';
import axios from "axios";
require('dotenv').config()

const PeoplesController = (props) => {
  const [peoples, setPeoples] = useState([]);
  const [prev, setPrev] = useState([]);
  const [next, setNext] = useState([]);
  const [search, setSearch] = useState([]);
  const history = useHistory();
  const baseUrl = process.env.REACT_APP_BASE_URL
  const dispatch = useDispatch();

  useEffect(() => {
     
    getAllPeoples(`${baseUrl}/people/`);
  }, []);

  const getAllPeoples = (url) => {
    axios
      .get(url, { headers: { "content-type": "application/json" } })
      .then((res) => {
        const { data } = res;
        console.log(data)
        setPeoples(data.data.peoples);
        setPrev(data.data.previous)
        setNext(data.data.next)
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {});
  };

  const searchPeoples = (search) => {
    getAllPeoples(`${baseUrl}/people/?search=${search}`);
  };

  const paginate = (url) => {
    getAllPeoples(`${baseUrl}/${url}`);
  };

  const openDetails=(obj)=>{
    dispatch({type: obj});
    history.push("/person");
  }

  return (
    <React.Fragment>
      <PeoplesView
        peoples={peoples}
        searchPeoples={searchPeoples}
        paginate={paginate}
        openDetails={openDetails}
        prev={prev}
        next={next}

      />
    </React.Fragment>
  );
};

export default PeoplesController;
