import React, {useState, useEffect} from 'react'
import PersonView from '../view/PersonView'
import { useHistory } from "react-router-dom";
import {useSelector} from 'react-redux';
import axios from "axios";
require('dotenv').config()

const PersonController = (props) =>{
    const history = useHistory();
    const [data, setData] = useState([])
    const baseUrl = process.env.REACT_APP_BASE_URL
    const obj = useSelector(state=>state.obj);
    
    useEffect(()=>{
       
        console.log(obj)
        // getPerson(`${baseUrl}/people/`)
    }, [])

    // const getPerson=()=>{
    //     axios
    //   .get(url, { headers: { "content-type": "application/json" } })
    //   .then((res) => {
    //     const { data } = res;
    //     console.log(data)
    //     setPeoples(data.data.peoples);
    //     setPrev(data.data.previous)
    //     setNext(data.data.next)
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   })
    //   .finally(() => {});
    // }

    const cancel=()=>{
        history.push("/");
    }

    return (
        <React.Fragment>
            <PersonView 
                cancel={cancel}
            />
        </React.Fragment>
    )
}

export default PersonController