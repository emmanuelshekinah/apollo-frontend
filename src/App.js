import {
  Switch,
  Route,
  // Redirect,
  // useHistory,
  BrowserRouter as Router,
} from "react-router-dom";
import Peoples from "./components/Peoples";
import Person from "./components/Person";
import { createStore } from "redux";
import { Provider } from "react-redux";
import rootReducer from "./components/utils/rootReducer";

const store = createStore(rootReducer);

function App() {
  const mystyle = {
    padding: "50px",
  };

  return (
    <div className="jumbotron p-8 m-8" style={mystyle}>
      <Provider store={store}>
        <div className="App">
          <Router>
            <Switch>
              <Route path="/" exact>
                <Peoples />
              </Route>
              <Route path="/person" exact>
                <Person />
              </Route>
            </Switch>
          </Router>
        </div>
      </Provider>
    </div>
  );
}

export default App;
